﻿using System.Collections.Generic;

namespace GoogleMapsTestweb.Models
{
	public class MapDataModel
	{
		public PinModel MapCenter { get; set; }

		public IEnumerable<PinModel> Pins { get; set; } 
	}
}
﻿using System;
using System.Linq;
using System.Web;

namespace GoogleMapsTestweb.Models
{
	public class PinModel
	{
		public double Lat { get; set; }

		public double Lon { get; set; }

		public string HtmlContent { get; set; }

		public string Title { get; set; }
	}
}
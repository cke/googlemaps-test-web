﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoogleMapsTestweb.Models;

namespace GoogleMapsTestweb.Controllers
{
	public class HomeController : Controller
	{
		private const int MapPointsCount = 50;

		private const string PinContentFormat = "<div class=\"pin-content\"><h3>Details for pin at lat {0} / lon {1}</h3><p>Here are the details for a pin.<br />This Text is just plain HTML with <strong>styling</strong>.</p></div>";

		private const string PinTitleFormat = "Title for position {0} / {1}";

		public ActionResult Index()
		{
			ViewBag.Message = "Here's a google map with random pins.";

			var model = new MapDataModel();

			model.MapCenter = new PinModel { Lat = -34.397, Lon = 150.644 };

			var rnd = new Random(Guid.NewGuid().GetHashCode());
			var pinList = new List<PinModel>();

			for (int index = 0; index < MapPointsCount; index++)
			{
				// just some random lat-lon generation
				var lat = rnd.Next(0, 40) + rnd.NextDouble();
				var lon = rnd.Next(0, 40) + rnd.NextDouble();

				pinList.Add(new PinModel
					{
						Lat = lat,
						Lon = lon,
						Title = string.Format(CultureInfo.CurrentCulture, PinTitleFormat, lat, lon),
						HtmlContent = string.Format(CultureInfo.CurrentCulture, PinContentFormat, lat, lon)
					});
			}

			model.Pins = pinList;

			return View(model);
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}
